﻿namespace dataSorter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupDirectory = new System.Windows.Forms.GroupBox();
            this.textDirectory = new System.Windows.Forms.TextBox();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.groupConvention = new System.Windows.Forms.GroupBox();
            this.textConvention = new System.Windows.Forms.TextBox();
            this.buttonRun = new System.Windows.Forms.Button();
            this.groupExtension = new System.Windows.Forms.GroupBox();
            this.radioNewExt = new System.Windows.Forms.RadioButton();
            this.radioSameExt = new System.Windows.Forms.RadioButton();
            this.textExtension = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.versionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.formStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupItems = new System.Windows.Forms.GroupBox();
            this.textSelectedExt = new System.Windows.Forms.TextBox();
            this.radioSelective = new System.Windows.Forms.RadioButton();
            this.radioAll = new System.Windows.Forms.RadioButton();
            this.groupDirectory.SuspendLayout();
            this.groupConvention.SuspendLayout();
            this.groupExtension.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupDirectory
            // 
            this.groupDirectory.Controls.Add(this.textDirectory);
            this.groupDirectory.Controls.Add(this.buttonBrowse);
            this.groupDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupDirectory.Location = new System.Drawing.Point(55, 47);
            this.groupDirectory.Name = "groupDirectory";
            this.groupDirectory.Size = new System.Drawing.Size(509, 116);
            this.groupDirectory.TabIndex = 0;
            this.groupDirectory.TabStop = false;
            this.groupDirectory.Text = "Choose Directory";
            // 
            // textDirectory
            // 
            this.textDirectory.Location = new System.Drawing.Point(135, 47);
            this.textDirectory.Name = "textDirectory";
            this.textDirectory.Size = new System.Drawing.Size(356, 26);
            this.textDirectory.TabIndex = 1;
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(26, 42);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(86, 37);
            this.buttonBrowse.TabIndex = 0;
            this.buttonBrowse.Text = "Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // groupConvention
            // 
            this.groupConvention.Controls.Add(this.textConvention);
            this.groupConvention.Location = new System.Drawing.Point(55, 194);
            this.groupConvention.Name = "groupConvention";
            this.groupConvention.Size = new System.Drawing.Size(509, 100);
            this.groupConvention.TabIndex = 1;
            this.groupConvention.TabStop = false;
            this.groupConvention.Text = "Choose Naming Convention";
            // 
            // textConvention
            // 
            this.textConvention.Location = new System.Drawing.Point(26, 44);
            this.textConvention.Name = "textConvention";
            this.textConvention.Size = new System.Drawing.Size(465, 26);
            this.textConvention.TabIndex = 0;
            this.textConvention.Text = "* bottles of beer on the wall";
            // 
            // buttonRun
            // 
            this.buttonRun.Location = new System.Drawing.Point(460, 469);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(103, 129);
            this.buttonRun.TabIndex = 2;
            this.buttonRun.Text = "Run";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // groupExtension
            // 
            this.groupExtension.Controls.Add(this.radioNewExt);
            this.groupExtension.Controls.Add(this.radioSameExt);
            this.groupExtension.Controls.Add(this.textExtension);
            this.groupExtension.Location = new System.Drawing.Point(55, 461);
            this.groupExtension.Name = "groupExtension";
            this.groupExtension.Size = new System.Drawing.Size(399, 137);
            this.groupExtension.TabIndex = 3;
            this.groupExtension.TabStop = false;
            this.groupExtension.Text = "Choose Final File Extension";
            // 
            // radioNewExt
            // 
            this.radioNewExt.AutoSize = true;
            this.radioNewExt.Location = new System.Drawing.Point(26, 82);
            this.radioNewExt.Name = "radioNewExt";
            this.radioNewExt.Size = new System.Drawing.Size(184, 24);
            this.radioNewExt.TabIndex = 2;
            this.radioNewExt.Text = "Change to extension:";
            this.radioNewExt.UseVisualStyleBackColor = true;
            this.radioNewExt.CheckedChanged += new System.EventHandler(this.radioNewExt_CheckedChanged);
            // 
            // radioSameExt
            // 
            this.radioSameExt.AutoSize = true;
            this.radioSameExt.Checked = true;
            this.radioSameExt.Location = new System.Drawing.Point(26, 42);
            this.radioSameExt.Name = "radioSameExt";
            this.radioSameExt.Size = new System.Drawing.Size(189, 24);
            this.radioSameExt.TabIndex = 1;
            this.radioSameExt.TabStop = true;
            this.radioSameExt.Text = "Use current extension";
            this.radioSameExt.UseVisualStyleBackColor = true;
            this.radioSameExt.CheckedChanged += new System.EventHandler(this.radioSameExt_CheckedChanged);
            // 
            // textExtension
            // 
            this.textExtension.Enabled = false;
            this.textExtension.Location = new System.Drawing.Point(228, 81);
            this.textExtension.Name = "textExtension";
            this.textExtension.Size = new System.Drawing.Size(136, 26);
            this.textExtension.TabIndex = 0;
            this.textExtension.Text = ".png";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(617, 33);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.versionToolStripMenuItem,
            this.infoToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(74, 29);
            this.helpToolStripMenuItem.Text = "About";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(154, 30);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // versionToolStripMenuItem
            // 
            this.versionToolStripMenuItem.Name = "versionToolStripMenuItem";
            this.versionToolStripMenuItem.Size = new System.Drawing.Size(154, 30);
            this.versionToolStripMenuItem.Text = "Version";
            this.versionToolStripMenuItem.Click += new System.EventHandler(this.versionToolStripMenuItem_Click);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(154, 30);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 636);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(617, 30);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "Inputing Parameters";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // formStatus
            // 
            this.formStatus.Name = "formStatus";
            this.formStatus.Size = new System.Drawing.Size(177, 25);
            this.formStatus.Text = "Inputting Parameters";
            // 
            // groupItems
            // 
            this.groupItems.Controls.Add(this.textSelectedExt);
            this.groupItems.Controls.Add(this.radioSelective);
            this.groupItems.Controls.Add(this.radioAll);
            this.groupItems.Location = new System.Drawing.Point(55, 324);
            this.groupItems.Name = "groupItems";
            this.groupItems.Size = new System.Drawing.Size(509, 121);
            this.groupItems.TabIndex = 6;
            this.groupItems.TabStop = false;
            this.groupItems.Text = "Choose Items to Edit";
            // 
            // textSelectedExt
            // 
            this.textSelectedExt.Enabled = false;
            this.textSelectedExt.Location = new System.Drawing.Point(247, 75);
            this.textSelectedExt.Name = "textSelectedExt";
            this.textSelectedExt.Size = new System.Drawing.Size(243, 26);
            this.textSelectedExt.TabIndex = 2;
            // 
            // radioSelective
            // 
            this.radioSelective.AutoSize = true;
            this.radioSelective.Location = new System.Drawing.Point(26, 76);
            this.radioSelective.Name = "radioSelective";
            this.radioSelective.Size = new System.Drawing.Size(215, 24);
            this.radioSelective.TabIndex = 1;
            this.radioSelective.Text = "Only items with extension:";
            this.radioSelective.UseVisualStyleBackColor = true;
            this.radioSelective.CheckedChanged += new System.EventHandler(this.radioSelective_CheckedChanged);
            // 
            // radioAll
            // 
            this.radioAll.AutoSize = true;
            this.radioAll.Checked = true;
            this.radioAll.Location = new System.Drawing.Point(26, 36);
            this.radioAll.Name = "radioAll";
            this.radioAll.Size = new System.Drawing.Size(173, 24);
            this.radioAll.TabIndex = 0;
            this.radioAll.TabStop = true;
            this.radioAll.Text = "All items in directory";
            this.radioAll.UseVisualStyleBackColor = true;
            this.radioAll.CheckedChanged += new System.EventHandler(this.radioAll_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 666);
            this.Controls.Add(this.groupItems);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupExtension);
            this.Controls.Add(this.buttonRun);
            this.Controls.Add(this.groupConvention);
            this.Controls.Add(this.groupDirectory);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Data Sorter";
            this.groupDirectory.ResumeLayout(false);
            this.groupDirectory.PerformLayout();
            this.groupConvention.ResumeLayout(false);
            this.groupConvention.PerformLayout();
            this.groupExtension.ResumeLayout(false);
            this.groupExtension.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupItems.ResumeLayout(false);
            this.groupItems.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupDirectory;
        private System.Windows.Forms.TextBox textDirectory;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.GroupBox groupConvention;
        private System.Windows.Forms.TextBox textConvention;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.GroupBox groupExtension;
        private System.Windows.Forms.TextBox textExtension;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel formStatus;
        private System.Windows.Forms.GroupBox groupItems;
        private System.Windows.Forms.TextBox textSelectedExt;
        private System.Windows.Forms.RadioButton radioSelective;
        private System.Windows.Forms.RadioButton radioAll;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem versionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.RadioButton radioNewExt;
        private System.Windows.Forms.RadioButton radioSameExt;
    }
}

