﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace dataSorter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            // OpenFileDialog fd = new OpenFileDialog();
            // if(fd.ShowDialog() == DialogResult.OK)
            // {
            //     textDirectory.Text = fd.File;
            // }
            var fd = new FolderBrowserDialog();
            if(fd.ShowDialog() == DialogResult.OK)
            {
                textDirectory.Text = fd.SelectedPath;
            }
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {



            bool isSelective = false;
            string dir = textDirectory.Text;
            string conv = textConvention.Text;
            bool newExt = false;
            string ext = textExtension.Text;
            if (radioNewExt.Checked)
                newExt = true;
            int indexForNum = 0;
            string selectedExt = null;
            bool foundIterateChar = false;


            if (radioSelective.Checked)
            {
                isSelective = true;
                selectedExt = textSelectedExt.Text;
            }

            if(!checkBeforeRun(dir, conv, isSelective, selectedExt, newExt, ext))
            {
                MessageBox.Show("Some parameters are unfilled / invalid");
                formStatus.Text = "Aborted.";
                return;
            }

            formStatus.Text = "Running.";

            for(int i = 0; i < conv.Length; i++)
            {
                char letter = conv[i];

                if(letter == '*')
                {
                    indexForNum = i;
                    formStatus.Text = ("Found iterate Char");
                    foundIterateChar = true;
                }
            }

            if (!foundIterateChar)
            {
                MessageBox.Show("No \'*\' was found in your naming convention. Aborting.");
                formStatus.Text = "Aborted.";
            }

            DirectoryInfo d = new DirectoryInfo(@dir);
            FileInfo[] infos = d.GetFiles();

            conv.Replace(conv[indexForNum], '0');


            int count = 0;
            for(int i = 0; i < infos.Length; i++)
            {
                FileInfo f = infos[i];
                if (!isSelective || Path.GetExtension(f.FullName) == selectedExt)
                {
                    string temp = conv;
                    string _ = count.ToString();
                    temp = temp.Replace(temp[indexForNum], ' ');
                    temp = temp.Insert(indexForNum, _);
                    formStatus.Text = ("Renaming File: " + temp);
                    if (newExt)
                    {
                        File.Move(f.FullName, (dir + "\\" + temp + ext));
                    } else
                    {
                        File.Move(f.FullName, (dir + "\\" + temp + Path.GetExtension(f.FullName)));
                    }
                    count += 1;
                }
            }
            formStatus.Text = "Done.";


        }

        private bool checkBeforeRun(string dir, string conv, bool selective, string selectedExt, bool useNewExt, string newExt)
        {
            if(dir == "")
            {
                return false;
            }
            else if(conv == "")
            {
                return false;
            }
            else if(selective && selectedExt == "")
            {
                return false;
            }
            else if(useNewExt && newExt == "")
            {
                return false;
            }

            return true;
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void radioAll_CheckedChanged(object sender, EventArgs e)
        {
            if(radioAll.Checked)
            {
                textSelectedExt.Enabled = false;
            }
        }

        private void radioSelective_CheckedChanged(object sender, EventArgs e)
        {
            if (radioSelective.Checked)
            {
                textSelectedExt.Enabled = true;
            }
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Use the Character \'*\' to show where to place the number in your filename.");
        }

        private void versionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Version 0.0.3");
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This tool is used to mass rename items in a given directory in order to number them in a unique way.");
        }

        private void radioSameExt_CheckedChanged(object sender, EventArgs e)
        {
            if (radioSameExt.Checked)
            {
                textExtension.Enabled = false;
            }
        }

        private void radioNewExt_CheckedChanged(object sender, EventArgs e)
        {
            if (radioNewExt.Checked)
            {
                textExtension.Enabled = true;
            }
        }

        private void progressNaming_Click(object sender, EventArgs e)
        {

        }
    }
}
